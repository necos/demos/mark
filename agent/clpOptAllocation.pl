%%% Selection Mechanism in CLP (Prolog)
%%% Code below solves a simple resource allocation problem
%%% where vdus are allocated to clusters, yielding the minimum cost.
%%% Author: Ilias Sakellariou
%%% Date Aug 2019, NECOS Project
%%% Version 0.2

:-module(clpOptAllocation,[assertlist/1,cluster_allocs/3,clear_all_resource_info/0]).

:-use_module(library(clpfd)).

:-dynamic cluster_avail/3.
:-dynamic resource_req/5.
:-dynamic cluster_test_cost/3.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% DataBase Management Predicates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%! clear_all_resource_info
% clear_all_resource_info/0
% Clears all dynamic DataBase Predicates used in the
% Optimization.

clear_all_resource_info:-
    retractall(cluster_avail(_,_,_)),
    retractall(resource_req(_,_,_,_,_)).

%! assertlist(+ListofTerms) is det
% Asserts a list of terms in the module's database.
% The predicate, receives a list of terms that contain the information
% of the problem.
%
% For this to work properly twoo kinds of terms have to be asserted:
%
assertlist([]).
assertlist([Item|Rest]):-
    assertz(Item),
    !,
    assertlist(Rest).


%! cluster_allocs(-OutTerms,-OutCost,++Startegy)
% The main solution predicate of the optimized allocation.
%
% @arg OutTerms is a list of terms in the form (VDU,Nodes,ClusterName)
% that is the output of the Optimization Process. The latter works on information
% found in terms that have been asserted with the assertlist/1 predicate.
% @arg OutCost is the optimum cost (minimum) from for the allocations
% @arg Startegy is a list of search strategies (e.g. [ffc]) used by the optimizer
% to find the best solution.
cluster_allocs(OutTerms,OutCost,Strategy):-
    setof(V,vdu_name(V),VDUS),
    length(VDUS,NumVdus),
    cluster_vars(NumVdus,ClusterAllocs),
    constraints_on_vdus(VDUS,Allocs,Terms,ClusterAllocs,Cost),
    once(labeling([min(Cost)|Strategy],Allocs)),

    % preparing the output.
    findall((VDU,Nodes,CLUSTER),
       (member((VDU,Nodes,CL_NUM),Terms),
        cluster_avail(CLUSTER,CL_NUM,_)),
          OutTerms),
    OutCost is Cost/1000.


%! vdu_name(?Name)
% Unifies Name with the name of a VDU obtained from the
% resource_req/5 terms.
% Needed for setof/3
vdu_name(Name):-
    resource_req(Name,_,_,_,_).

%%% constraints_on_vdus/5
%%% constraints_on_vdus(VDUS,Allocs,ClusterAllocs,Cost)
%%% Constraints Allocations to VDUS.
%%% VDUS are the names of the VDUS to allocate.
%%% Allocs are the decision variables which assign each VDU to a CLUSTER
%%% ClusterAllocs is the list of terms to be used as Output
%%% Cost is the total cost.
constraints_on_vdus([],[],[],_,0).
constraints_on_vdus([VDU|RestVDUS],[Allocation|Allocs],[(VDU,Nodes,Allocation)|RestTerms],[ClusterAllocs|RestCLAllocs], TotalCost):-
  %% find where each VDU can be allocated
  once(resource_req(VDU,_,_,Nodes,_)),
  findall(ClNum,resource_req(VDU,_,ClNum,_,_),ClustersN),
  %% Find the cost of each Allocation
  findall(C,(resource_req(VDU,_,_,_,CInt),C is round(CInt * 1000)),ClustersCost),
  element(I,ClustersN,Allocation),
  element(I,ClustersCost,Cost),
  % Which cluster var is to be assigned the VDU.
  element(Allocation,ClusterAllocs,Nodes),
  % So one gets the Nodes and the others get 0.
  % Good old fashioned cardinaltiy Constraint
  length(ClusterAllocs,NumCL),RCL is NumCL - 1,
  global_cardinality(ClusterAllocs,[Nodes-1,0-RCL]),
  constraints_on_vdus(RestVDUS,Allocs,RestTerms,RestCLAllocs,RestCost),
  TotalCost #= Cost + RestCost.

%%% cluster_vars/2
%%% custer_vars(NumVDUS,Struct)
/*
The predicate creates a list of length NumVDUS for each cluster,
respecting the cluster numbering (see above). This list represents the allocation of
a VDU to the specific cluster, such that if the value of the ith var is
different than zero, then the ith VDU is allocated to this cluster, and
zero otherwise. This different than zero values are the number of nodes the ith
VDU has. Obviously the sum of this list fro each cluster must be less or equal than its
capacity.
The pred returns the transpose of this list of lists.
*/
%%% Cluster Capacity constraints
cluster_vars(NumVdus,Struct):-
    setof(N,clusterNum(N),List),
    set_2D_array(List,NumVdus,TempStruct),
    transpose(TempStruct,Struct).

%%% clusterNum/1
%%% clusterNum(ClusterName)
%%% Succeeds by unifying the cluster name of a cluster
clusterNum(N):-
    cluster_avail(_,N,_).

%%% set_2D_array/3
%%% set_2D_array(ClusterNumbers,NumVDUS,ListofLists)
%%% See description above.
set_2D_array([],_,[]).
set_2D_array([N|List],Num,[Vars|Struct]):-
    length(Vars,Num),
    cluster_avail(_,N,Max),
    %% No need to check sum If I take a large VDU.
    Vars ins 0..Max,
    sum(Vars,#=<,Max),
    set_2D_array(List,Num,Struct).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code used to produce a variety of problems to test the Optimization
%%% test Predicates
%%% Availability of Clusters (nodes)
%%% This is the initial test.
/*
cluster_avail('ZOTAC',1,1).
cluster_avail('APU 1d4',2,5).
cluster_avail('DSS/MOBILE',3,10).
cluster_avail('SERVER1P',4,0).
cluster_avail('SERVER5P',5,4).
cluster_avail('SERVER1G2X',6,6).

%%% Resource request (VDU,ClusterName,ClusterNum,NodesRequired,Price)
resource_req(load_balancer,'ZOTAC',1,1,0.01).
resource_req(load_balancer,'APU 1d4',2,1,0.015).
resource_req(load_balancer,'SERVER1P',4,1,0.12).
resource_req(load_balancer,'SERVER5P',5,1,0.21).

resource_req(web_server_VM,'ZOTAC',1,5,0.05).
resource_req(web_server_VM,'APU 1d4',2,5,0.075).

resource_req(orchestrator,'ZOTAC',1,1,0.01).
resource_req(orchestrator,'APU 1d4',2,1,0.015).
resource_req(orchestrator,'SERVER1P',4,1,0.12).
resource_req(orchestrator,'SERVER5P',5,1,0.21).
*/


%!  test_problem(++NumVDUs,++VDUSize,++MaxMatchings,++NumClusters,++MAxAv)
%  Predicate generates a problem instance with certain characteristics,
%  regarding the size of the problem.
%
% @arg NumVDUS the number of VDU in the test case generated
% @arg VDUSize is the number of nodes in each VDU
% @arg MaxMatching is the number of Max Clusters that a VDU can match
% @arg NumClusters is the number of clusters
% @arg MAxAv is the maximum Availability of each clusters
% Obviously maximum values mean that a random value is selected
% from the value given.


test_problem(NumVDUS,VDUSize,MaxMatchings,NumClusters,MaxAv):-
   clear_all_resource_info,
   retractall(cluster_test_cost(_,_,_)),
   create_clusters(NumClusters,MaxAv,AVList,CostList),
   assertlist(AVList),
   assertlist(CostList),
   create_vdu_demands(NumVDUS,VDUSize,MaxMatchings,Reqs),
   assertlist(Reqs).


%%%  Creating cluster test cases.
create_clusters(0,_,[],[]):-!.
create_clusters(N,Max,[cluster_avail(Name,N,Nodes)|Rest],[cluster_test_cost(Name,N,Cost)|RestCost]):-
    N > 0,
    atom_concat(clust,N,Name),
    Nodes is 1 + random(Max),
    NN is N - 1,
    Cost is random(100) / 1000,
    create_clusters(NN,Max,Rest,RestCost).

create_vdu_demands(0,_,_,[]).
create_vdu_demands(NumVDU,VDUSize,MaxMatchings,Reqs):-
     NumVDU > 0,
     atom_concat(vdu,NumVDU,Name),
     Nodes is 1 + random(VDUSize),
     findall(CL,(cluster_avail(CL,_,CLNodes),CLNodes >= Nodes),Candidates),
     length(Candidates,LenCand),
     Matches is 1 + random(min(MaxMatchings,LenCand)),
     select_random(Matches,Candidates,ClusterMatches),
     singleVDUDemand(ClusterMatches,Name,Nodes,VDUReq),
     NewNumVDU is NumVDU - 1,
     create_vdu_demands(NewNumVDU,VDUSize,MaxMatchings,ListReqs),
     append(VDUReq,ListReqs,Reqs).

singleVDUDemand([],_,_,[]):-!.
singleVDUDemand([CL|Rest],Name,Nodes,[resource_req(Name,CL,Num,Nodes,Cost)|Reqs]):-
    cluster_test_cost(CL,Num,UnitCost),
    Cost is Nodes * UnitCost,
    singleVDUDemand(Rest,Name,Nodes,Reqs).


%%% Select random Elements
select_random(0,_,[]):-!.
select_random(Num,List,[X|Rest]):-
  Num > 0,
  length(List,N),
  Pos is random(N),
  nth0(Pos,List,X,RestList),
  NNum is Num - 1,
  select_random(NNum,RestList,Rest).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
