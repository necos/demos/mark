
# Testbed Information Extractor
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Polyxronis Valsamas, Sarantis Kalafatidis, Ilias Sakellariou, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import argparse
import datetime
import csv
#from jsonsocket import Client
import time
import json
from yaml2json import *
import requests # sudo apt-get install python-requests
#import xmltodict # sudo apt-get install python-xmltodict
import xml.etree.ElementTree as ET
#from config import *
import random


def createFileFromString (filename, content):
    f = open(filename,'w')
    f.write(content)
    f.close()

def createStringFromFile (filename):
    f = open(filename,'r')
    content = f.read()
    f.close()
    return content

def appendFile (filename, content):
    f = open(filename, 'a')
    f.write (content)
    f.close()

def extractNodeInformation (inputString, testbedName, searchType):
    result=''

    #tree = ET.parse(fileName)
    #root=tree.getroot()

    root = ET.fromstring(inputString)
    #print (tree)
    #lst = tree.findall('node')
    #print (lst)

    nodes=root.findall ('{http://www.geni.net/resources/rspec/3}node')

    # calculate statistics
    totalNodes=0
    availableNodes=0
    matchedNodes=0


    for node in nodes:
        #print (node.attrib)
        # return values
        componentId=''
        available=''
        hardwareTypes=[]

        totalNodes=totalNodes+1

        componentId = node.attrib.get('component_id')
        if (node.find('{http://www.geni.net/resources/rspec/3}available') is not None):
            available = node.find('{http://www.geni.net/resources/rspec/3}available').get('now')
            if (available=='true'):
                availableNodes=availableNodes+1
        else:
            available = 'false'
        for hardwareType in node.findall ('{http://www.geni.net/resources/rspec/3}hardware_type'):
            hardwareTypes.append(hardwareType.get('name'))
            #for info in hardwareType:
            #    print (info.tag)

            #for info in hardwareType.findall ('{http://www.geni.net/resources/rspec/3}info'):
            #    print (info.tag)
        if searchType in hardwareTypes:
            matchedNodes=matchedNodes+1

        result += testbedName+"; "+componentId+"; "+available+"; "+str(hardwareTypes)+"\n"
    ##print ("testbed "+testbedName+ " nodes "+str(totalNodes)+" available "+str(availableNodes)+" matched "+str(matchedNodes))

    return result
        #print di
def calculate_totals (search_id, filename):
    #hardware_type = "['pcgen02-1p', 'pc', 'pcgen02-1p-vm', 'pcvmgen02-1p', 'pcvm', 'lan']"
    #hardware_type = "['dl360', '', 'pc', 'delay', 'delay-dl360', 'bridge', 'bridge-dl360', 'dl360-vm', '-vm', 'pcvm', 'lan']"
    resourcesFile = open (filename,'r')

    total_count=0
    available_count=0
    for line in resourcesFile:
        fields=line.split(';')
        available=fields[2].strip()
        hw_type=fields[3].strip()
        #print (str(available))
        if (hw_type==search_id):
            #print ("available "+str(available)+" hw_type "+hw_type)
            # hw_type matched
            total_count=total_count+1
            if (available=="true"):
                available_count=available_count+1

    return (total_count, available_count,search_id)

def create_parser_resourses(testbedCategory):
    output = {}
    output['testbed']=[]


    NewfileHandle = open('resources.txt','r')
    # parse data from resources.txt
    #fields = line.split(';')
    #current_id = fields[0]
    #print current_id


    fileHandle = open('testbed-resources.txt','r')

    current_id = None
    #print testbedCategory
    for line in fileHandle:
        #print (line)

        fields = line.split(';')
        #print str(testbedCategory)+"Utah"
        #print ("field is")
        #print fields[0]
        #print type(args.testbedCategory)
        #print args.testbedCategory
        #print fields[0]
        #print testbedCategory
        if (fields[0] == testbedCategory):
        #if (fields[0] == args.testbedCategory): #"CloudLabUtah": #str(testbedCategory)+"Utah":

            #print ("inside loop")
            testbed_id=fields[0]
            description = "Fed4Fire testbed "+testbed_id+"."
            country=fields[1]
            location=fields[2]
            name=fields[3]
            rspec_hwtype=fields[4]
            #print ("rspec_hwtype")
            #print fields[4]
            totals = calculate_totals (fields[4].strip(), 'resources.txt')
            ##print totals
            #print ("Total:"+str(totals[0])+" available:"+str(totals[1])+" Search_id: "+str(totals[2]))
            total_nodes=totals[0]
            available_nodes=totals[1]
            cpu_model=fields[7]
            cpu_vendor=fields[8]
            cpu_ghz=fields[9]
            cpu_number=fields[10]
            cpu_cores=fields[11]
            total_cpu_cores=fields[12]
            storage_description=fields[13]
            min_storage_gb=fields[14]
            memory_mb=fields[15]
            nics_description=fields[16]
            nics_bw=fields[17]
        # each time the id changes, add a new nodes field
        # each row is one node_cluster




            if (current_id!=testbed_id):
                # start a new testbed
                testbed = {}
                testbed['testbed']={}
                testbed['testbed']['id']=testbed_id
                testbed['testbed']['description']=description
                # Added geographic Information
                testbed['testbed']['location']={"country": country, "continent": location.upper()}
                # adding connectivity Information
                testbed['testbed']["dc-slice-controller"] = {}
                testbed['testbed']["dc-slice-controller"]["dc-slice-provider"] = testbed_id
                ip_0 = str(random.randint(11,70))
                ip_8 = str(random.randint(1,255))
                ip_16 = str(random.randint(1,255))
                ip_24 = str(random.randint(2,255))
                testbed['testbed']["dc-slice-controller"]["ip"] = ".".join([ip_0,ip_8,ip_16,ip_24])
                testbed['testbed']["dc-slice-controller"]["port"] = random.randint(500,1024)
            #----------Create dc-slice-point-of-presence-----------
                testbed['testbed']["dc-slice-point-of-presence"] = {}
                testbed['testbed']["dc-slice-point-of-presence"]["pop-name"] = "Edge Router 1"
                testbed['testbed']["dc-slice-point-of-presence"]["ip"] = ".".join([ip_0,ip_8,ip_16,"1"])
                testbed['testbed']["dc-slice-point-of-presence"]["router-type"] = "Wired Router-cisco"
                testbed['testbed']["dc-slice-point-of-presence"]["reservation-protocol"] = "BGP"
                testbed['testbed']["dc-slice-point-of-presence"]["requirements"] = {}
                testbed['testbed']["dc-slice-point-of-presence"]["requirements"]["bandwidth_gb"] = random.randint(1,10)


                # Added testbed information regarding cost
                testbed['testbed']["cost_per_cluster"] = []
                testbed['testbed']['nodes']=[]
                output['testbed'].append(testbed)
                #print ("first if")
                #print current_id
                #print ("TEST "+str(current_id)+" "+testbed_id)
                # save previous testbed, if it exists
                # if (current_id is not None):
                # #    print ("second if")
                #     output['testbeds'].append(testbed)
                # # start a new testbed
                # testbed = {}
                # testbed['testbed']={}
                # testbed['testbed']['id']=testbed_id
                # testbed['testbed']['description']=description
                # testbed['testbed']['location']=location
                # testbed['testbed']['nodes']=[]
                # output['testbeds'].append(testbed)

                current_id = testbed_id
                #print current_id
                #print testbed
            #if total_nodes >0 :
            # start a new node_cluster
            node_cluster = {}
            node_cluster['node_cluster']= {}
            node_cluster['node_cluster']['name']=name
            node_cluster['node_cluster']['rspec_hwtype']=rspec_hwtype
            node_cluster['node_cluster']['total_nodes']=int(total_nodes)
            node_cluster['node_cluster']['available_nodes']=int(available_nodes) #int kanto meta
            node_cluster['node_cluster']['cpu_model']=cpu_model
            node_cluster['node_cluster']['cpu_vendor']=cpu_vendor
            node_cluster['node_cluster']['cpu_ghz']=float(cpu_ghz) #float kanto meta
            node_cluster['node_cluster']['cpu_number']=int(cpu_number)
            node_cluster['node_cluster']['cpu_cores']=int(cpu_cores)
            node_cluster['node_cluster']['total_cpu_cores']=int(total_cpu_cores)
            node_cluster['node_cluster']['storage_description']=storage_description
            node_cluster['node_cluster']['min_storage_gb']=int(min_storage_gb)
            node_cluster['node_cluster']['memory_mb']=int(memory_mb) * 1024
            node_cluster['node_cluster']['nics_description']=nics_description
            node_cluster['node_cluster']['nics_bw']=int(nics_bw)

            # append node_cluster to current testbed
            testbed['testbed']['nodes'].append (node_cluster)
            testbed['testbed']['cost_per_cluster'].append({'cluster':name, 'price':random.uniform(0.1,0.5)})

            #print testbed
            #print output
    fileHandle.close()
    return output

# import sched, time
#
# s = sched.scheduler(time.time, time.sleep)
# def do_something(sc):
#     print "Doing stuff..."
#     # do your stuff
#     s.enter(5, 1, do_something, (sc,))




parser = argparse.ArgumentParser()
#parser.add_argument('--Trigger',   action='store_True', help="Trigger or not")
parser.add_argument("--Trigger", help="Trigger or not",
                    action='store_true')
parser.add_argument("--retrieveLocally", help="To retrieve Resource Locally or through Fed4fire Api",
                     action='store_true')
parser.add_argument("--outputFile", help="Save retrieve resources",
                     nargs='?', default='resources.txt')
parser.add_argument("--testbedCategory", help="Category of Testbed",
                     nargs='?', default='allcategories')
parser.add_argument("--testbedName", help="Testbed name",
                     nargs='?', default='cloudlabUtah')
parser.add_argument("--hwtypeToLookup", help="Hardware Type to look up",
                     nargs='?', default='pc')
args = parser.parse_args()
#print args.Trigger
#print type(args.Trigger)

while args.Trigger:
    try:
         #print datetime.datetime.now()
         #print args.Trigger
         #print args.retrieveLocally
         #print args.outputFile
         #print args.testbedCategory
         #print args.hwtypeToLookup
         #print args.testbedName

         createFileFromString(args.outputFile,'')

         if (args.retrieveLocally):
             #to open a local json file (e.g., listresources.json)
             jsonString = yaml_to_json ('allcategories.json')
             jsonObject = json.loads(jsonString)
             #print 'ok'
         else:
             #lookup available test-beds and rspec urls
             if (args.testbedCategory=='allcategories'):
                 testbeds = requests.get('https://flsmonitor-api.fed4fire.eu/result?embed=true&enabled=true&last=&testdefinitionname=listResources')
             else:
                 testbeds = requests.get('https://flsmonitor-api.fed4fire.eu/result?embed=true&enabled=true&last=&testbedcategory='+args.testbedCategory+'&testdefinitionname=listResources')
                 #testbeds = requests.get('https://flsmonitor-api.fed4fire.eu/result?embed=true&enabled=1&last=&testbedcategory='+args.testbedCategory+'&testdefinitionname=listResources')
             if testbeds.status_code != 200:
                 # This means something went wrong.
                 raise ApiError('ERROR: {}'.format(resp.status_code))
             #print (json.dumps(json_object, sort_keys=True, indent=4))
             jsonObject=testbeds.json()

             # cache result locally
             createFileFromString (args.testbedCategory+".json", testbeds.text)

         for rspec in jsonObject:
             if 'rspecUrl' in rspec['results']:
                 testbedUrl=rspec['results']['testbed']
                 splitUrl=testbedUrl.split('/')
                 testbedName=splitUrl[len(splitUrl)-1]
                 if testbedName == args.testbedName:
                     #testbedUrl=rspec['results']['testbed']
                     #splitUrl=testbedUrl.split('/')
                     #testbedName=splitUrl[len(splitUrl)-1]
                     testbedRspecUrl=rspec['results']['rspecUrl']
                     fileName=testbedName+".rspec"

                     if (args.retrieveLocally):
                         #print ("Opening rspec for testbed "+testbedName+" filename: "+fileName)
                         #rspec = createStringFromFile (fileName)
                         #print (rspec)
                         rspecText = createStringFromFile (fileName)

                     else:
                         #print ("Downloading rspec for testbed "+testbedName+" from url: "+testbedRspecUrl)
                         #print ("Creating filename: "+fileName)
                         # Download rspec
                         rspec = requests.get (testbedRspecUrl)
                         if rspec.status_code != 200:
                             # This means something went wrong.
                             raise ApiError('ERROR: {}'.format(resp.status_code))

                         # Save rspec file
                         rspecText = rspec.text
                         createFileFromString (fileName, rspecText)

                     # create output
                     testbedNodeInformation = extractNodeInformation (rspecText, testbedName, args.hwtypeToLookup)
                     if (testbedNodeInformation is not None):
                         appendFile (args.outputFile, testbedNodeInformation)
                         #print (testbedName+", "+testbedNodeInformation)

                     #print ("*******************")
         args.Trigger=False
         #output=create_parser_resourses(args.testbedCategory)
         output=create_parser_resourses(args.testbedName)
         serialized= json.dumps(output, sort_keys=True, indent=3)
         createFileFromString(args.testbedName+'_resources.json', serialized)

         #print ("Done")


# #if args.opt_number:
# k=args.opt_number
# s.enter(k, 1, do_something, (s,))
# s.run()
# #else:
# #    DefaultSceduler = 10
# #    s.enter(DefaultSceduler, 1, do_something, (s,))
# #    s.run()

    except (SystemExit, KeyboardInterrupt):
            #print ("Exiting...")
            break
