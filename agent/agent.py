# DC Agent code
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Sarantis Kalafatidis, Polyxronis Valsamas, Ilias Sakellariou, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.


import sys
import os
import signal
import json
import subprocess
import pika


from pyPrologConnection import *
from marketplace_utilities import *

# Globals
def resource_filefrom_arg():
    if sys.argv[1] in F4F_TESTBEDS:
        return './testbedExtractor/'+sys.argv[1]+'_resources.json'
    else:
        return sys.argv[1]

F4F_TESTBEDS =["cloudlabUtah","cloudlabWisconsin","grid5000","vwall1","vwall2","wilab2"]
RESOURCE_FILE_NAME=resource_filefrom_arg()


###################################################
# signal section
def terminateAgent(p1,p2):
    sys.exit(1)

signal.signal(signal.SIGHUP, terminateAgent)
signal.signal(signal.SIGTERM, terminateAgent)
signal.signal(signal.SIGINT, terminateAgent)

###################################################

# Utility for setting the binding keys
# on Arg 1
def provider_binding_keys():
    #resource_json = loadFile(sys.argv[1])
    resource_json = loadFile(RESOURCE_FILE_NAME)
    location = (resource_json["testbed"][0])["testbed"]["location"]
    return [location["continent"]+".*", "*."+location["country"]]


RMQ_Conf = load_rabbit_config()
hostIP=RMQ_Conf['rabbitmq_ip'] #'127.0.0.1'
creds = pika.PlainCredentials(RMQ_Conf['rabbitmq_user'],RMQ_Conf['rabbitmq_pass'])

#----connecting with RabbitMQ server
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=hostIP, credentials = creds)
    )
channel= connection.channel()

#------Set exchange type------
channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

#----Create and declare a unic queue for each client---
result = channel.queue_declare('', exclusive=True)
queue_name = result.method.queue

#----Create binding (The relationship between exchange and a queue)
#binding_keys = sys.argv[2:]
#new_keys = provider_binding_keys()
#print("Computed Keys:", new_keys, "Old Keys", binding_keys)

binding_keys = provider_binding_keys()
print("Computed Keys:", binding_keys)
print("Resource Configuration File: ",sys.argv[1])


if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
        exchange='topic_logs', queue=queue_name, routing_key=binding_key)
    print(binding_key)

print(' [*] Waiting messages from broker. To exit press CTRL+C')

#---Callback method (what to do when i take a message)---
def callback(ch, method, props, body):
    #print(body)
    #print(" [x] %r:%r" % (method.routing_key, body))
    pr  = start_prolog('./sliceagent.pl')
    #load_testbed_info(sys.argv[1]) #'./singleTestBed.py')
    load_testbed_info(RESOURCE_FILE_NAME)
    # Aparently the messages are received in b'"form"
    final_message_dict = agentAnswer(body.decode())
    final_message = json.dumps(final_message_dict)
    print("----------------")
    #print(final_message)

    ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                             props.correlation_id),
                         body=final_message)





#------Basic consume (waiting for responce)-----
channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
#channel.basic_qos(prefetch_count=1)
