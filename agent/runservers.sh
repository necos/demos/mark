#!/bin/bash

# GETS all files automatically
FILES=`ls ./providers/dcProv*.json`
echo $FILES
for TEST in $FILES; do
  Name=$(python3 terminalName.py $TEST)
  xterm -fa 'Monospace' -fs 12 -T $Name +ls -xrm 'XTerm*selectToClipboard: true' -hold -e python3 -B agent.py $TEST &
done
xterm -hold
