%%% Slice Agent in Prolog
%%% Author: Ilias Sakellariou
%%% Date May 2019, NECOS Project
%%% Version 0.4

%% Libraries for loading json files
:-use_module(library(http/json)).
:-use_module(library(http/json_convert)).
:-use_module(library(http/http_json)).
%:-use_module(library(dicts)).

%% Optimization module
:-use_module(clpOptAllocation).

:-dynamic testbed/1.
:-dynamic request/1.
:-dynamic complete/1.
:-dynamic activator/1.
:-dynamic cost_per_cluster/2.
%% remove this.
:-dynamic slice/2.
:-dynamic req/1.

%%% Module for CLP Allocation


%%% Slice Agent Code.
%%% Loads the agent resource description from the file.
%%% The dictionary is both "returned" in the second argument
load_agentdb_file(FileName,OutDict,ok):-
	open(FileName,read,S,[]),
	json_read_dict(S,Dict,[value_string_as(atom)]),
	retract_info(testbed),
  [OutDict] = Dict.testbed,
	assert(testbed(OutDict)),
	assert_cost_model,
	close(S),
	!.

load_agentdb_file(_,_,failed).

%%% assert_cost_model/0
%%% exatracting information regarding the cost of each machine
%%% in the cluster.

assert_cost_model:-
		testbed(TestbedInfo),
		get_deep(['testbed','cost_per_cluster'],TestbedInfo,ListCosts),
		forall(member(ClCost,ListCosts),assert(cost_per_cluster(ClCost.cluster,ClCost.price))).

%% in case there is no cost.
assert_cost_model.

%%%% Loading the Request
%%% load_request_file(FileName,functor)
%%% loads a request file (Json Message)
%%% and asserts it under functor.
%%% This is for tesring ONLY
load_request_file(FileName,What):-
	open(FileName,read,S,[]),
	retract_info(What),
	json_read_dict(S,Dict,[value_string_as(atom)]),
	assert_info(What,Dict),
	close(S).

%%% Generalising the predicate in order to accommodate
%%% multiple files.
retract_info(What):-
		C=..[What,_],
		retractall(C).

assert_info(What,Info):-
  	C=..[What,Info],
		assert(C).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% answerAgent/2
%%% Main predicate called by Python.
%%% Iyts parses the json message to voncert it to a Prolog
%%% dictionary and then converts the answer (again dictionary) to
%%% a JSON message returned to python.
answerAgent(JSonMessage,Output):-
    atom_string(JSonMessage,File),
    open_string(File,Stream),
    json_read_dict(Stream,Dict,[value_string_as(atom)]),
    process_answer(Dict,DictOut),
    atom_json_dict(Output,DictOut,[value_string_as(string)]),
    write('ok'),nl.

%%% process_answer/2
%%% process_answer(QueryDictionary,AnswerDictionary)
%%% The Main process message. It receives a the generated dictionary that
%%% corresponds to the query and generates the Answer dictionary to be sent
%%% back to python, via a json message.
process_answer(D,Ans):-
	 answer_request_message(D,A),
	 update_request_answer(D,A,Ans).

%%%%%%%%%%%% Testing Code to be removed in Production Code
testing:-
    load_agentdb_file('./singleTestBed.py',_,S),
		write(S),nl,
		load_request_file('./request.json',req).


testing2:-
		req(Message),
		process_message(Message,VDU),
		findall(Match,(
		  testbed_resources(Node),
			match_vdu_to_cluster(VDU,Node,Match)
			),Matches),
    filter_matches(Matches,Succeeded,Failed),
		write('---- Succeeded --- '),nl,
		writelist(Succeeded),nl,
		write('---- Failed --- '),nl,
		writelist(Failed),nl.

process_message(Message,VDU):-
	get_deep(['dc-slice-part','vdus'],Message, Vdus),
  member(VDU,Vdus).

%%%%%%%%%%%%%%% End testing Code.


%%% answer_request_message/2
%%% answer_request_message(Request,Alllocs)
%%% On suceess, it reports back the allocations of vdus to clusters, along
%%% with any values that have to be reported back.
answer_request_message(Request,Allocs):-
  once(get_deep(['dc-slice-part','vdus'],Request, Vdus)),
  alloc_vdus(Vdus,All_Allocations),
	!,
	demo_print_allocs(All_Allocations),
	clp_allocation(All_Allocations,Allocs),
	%writelist(Allocs2),nl,
	%distribute_allocations(All_Allocations,Allocs),
	%write('-------FINAL ALLOCS--------'),nl,
	%writelist(Allocs),nl,
	true.

%%% distribute_allocations/2
%%% This is still naive. No Longer Used
%%% REMOVE in FINAL
distribute_allocations([],[]).
distribute_allocations([VDUAlloc|Rest],[A|RestAllocs]):-
	member(A,VDUAlloc),
	distribute_allocations(Rest,RestAllocs).
%%% End of code to delete


%%% Clp Allocation that takes into account Minimum Cost and Nodes availability.
%%% clp_allocation/2
%%% clp_allocation(All_Allocations,Allocs)
%%% All_Allocations are the potential VDU to cluser mappings
%%% Allocs is a VDU to cluster assignement that takes into account Cost
%%% and Availability.
clp_allocation(All_Allocations,Allocs):-
	clpOptAllocation:clear_all_resource_info,
	flatten(All_Allocations,FlatAllocations),
	%writelist(FlatAllocations),
	cluster_numbering(NumberedClusters),
	collect_cluster_available_nodes(Cluster_Availability_List,NumberedClusters),
	clpOptAllocation:assertlist(Cluster_Availability_List),
	%writelist(Cluster_Availability_List),
	collect_alloc_info(FlatAllocations,NumberedClusters,OptInfoAllocs),
	clpOptAllocation:assertlist(OptInfoAllocs),
	%writelist(OptInfoAllocs),nl,
	clpOptAllocation:cluster_allocs(OptimalAllocs,Cost,[ffc]),
	write('Selected: '),
	write(OptimalAllocs),nl,
	write('Cost: '), write(Cost),nl,
  findall(Alloc,complete_info(OptimalAllocs,FlatAllocations,Alloc),Allocs).

%%% complete_info/3
%%% complete_info(OptimalAllocs,FlatAllocations,Alloc)
%%% OptimalAllocs is a list of (VDU,Nodes,Cluster) terms.
%%% It "returns" the full Allocation info as the latter is found
%%% in the FlatAllocations list (dc_in_cluster(VDU,Cluster,Info) terms).
%%% On backtracking it will return all the Alloc(s) necessary for the answer.
complete_info(OptimalAllocs,FlatAllocations,Alloc):-
		member((VDU,_,Cluster),OptimalAllocs),
		Alloc =.. [dc_in_cluster,VDU,Cluster,_ListRes],
		once(member(Alloc,FlatAllocations)).


%%% collect_alloc_info/3
%%% collect_alloc_info(All_Allocations,NumCl_List,OptInfoAllocs)
%%% All_Allocations is the list of dc_in_cluster/3 terms representing potential
%%% Allocations
%%% NumCl_List is the list of numberings of clusters (needed for Optimization)
%%% OptInfoAllocs is the information needed to discover an optimal allocation.
%%% It scans all allocations and collects only interesting values to
%%% act as input in the CLP problem.
collect_alloc_info(All_Allocations,NumCl_List,OptInfoAllocs):-
	%member(One,All_Allocations),
	findall(resource_req(VDU,CLUSTER,Num,Nodes,Cost),
	    (member(dc_in_cluster(VDU,CLUSTER,ListAllocs),All_Allocations),
			 once(member(nodes(Nodes),ListAllocs)),
			 compute_cluster_cost(CLUSTER,Nodes,Cost),
			 member(cluster_num(Num,CLUSTER),NumCl_List)
			  ),
			   OptInfoAllocs).

%%% collect_cluster_available_nodes/2
%%% collect_cluster_available_nodes(Availability_List,NumCl_List)
%%% Collects the current availability in hosts of clusters in the provider.
collect_cluster_available_nodes(Availability_List,NumCl_List):-
		testbed(TestBed),
		get_deep(['testbed','nodes'],TestBed,ListClusters),
		findall(cluster_avail(CL,Num,Nodes),
		    (member(ClusterDict,ListClusters),
				 get_deep(['node_cluster','available_nodes'],ClusterDict,Nodes),
				 get_deep(['node_cluster','name'],ClusterDict,CL),
				 member(cluster_num(Num,CL),NumCl_List)
				 ),Availability_List).

%%% cluster_numbering/1
%%% cluster_numbering(Preds)
%%% Numbers the clusters in order to translate the problem in an Opt problem.
cluster_numbering(Preds):-
 		testbed(TestBed),
		get_deep(['testbed','nodes'],TestBed,ListClusters),
		findall(Cl,
				(	member(ClusterDict,ListClusters),
			 		get_deep(['node_cluster','name'],ClusterDict,Cl)
			 	 ), Cluster_List),
		number_clusters(1,Cluster_List,Preds),!.

number_clusters(_,[],[]).
number_clusters(N,[Cl|Clusters],[cluster_num(N,Cl)|Rest]):-
	  NN  is N + 1,
		number_clusters(NN,Clusters,Rest).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Updating the answer.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% update_request_answer/3
%%% update_request_answer(Request,Allocations,Ans)
%%% Request is the original request coming from the Broker,
%%% whereas Allocs are the allocated clusters with the data,
%%%
update_request_answer(Request,Allocs,Ans):-
	duplicate_term(Request,Ans),
	once(get_deep(['dc-slice-part','vdus'],Ans, Vdus)),
  insertAllocs(Allocs,Vdus,NewVdus),
	set_deep(['dc-slice-part','vdus'],Ans,NewVdus),
	total_offer_cost(Allocs,_Num,Cost),
	set_deep(['dc-slice-part','cost'],Ans,Cost),
  update_controller_pop_info(Ans).
	%write(total_offer_cost(l,Num,Cost)).
	%writelist(Allocs),nl.

%%% Insert allocs for different slice parts.
%%% insertAllocs/3
%%% insertAllocs(Dc_in_clusterAllocs,RequestVDUS,UpdatedVDUs)
%%% IS: Since this is using destructive assignment (apologies to the LP community)
%%% the third argument is uneccessary (to be removed in a latter Version)

insertAllocs([],Vdus,Vdus).
insertAllocs([dc_in_cluster(VDUID,_,ListOfValues)|Rest],Vdus,NewVdus):-
   % gets the dictionary of the vdu
   member(V,Vdus),
	 % its the correct one.
	 get_deep(['dc-vdu',id],V,VDUID),
	 !,
	 set_all_atts(V,ListOfValues),
	 insertAllocs(Rest,Vdus,NewVdus).

%%% Updating Values
%%% set_all_atts/2
%%% set_all_atts(VDU_Dictionary,ValuesToChange)
set_all_atts(_,[]).

set_all_atts(V,[nodes(Nodes)|ListOfValues]):-
	set_deep(['dc-vdu','host-count-in-dc'],V,Nodes),
	set_all_atts(V,ListOfValues).

set_all_atts(V,[succeeded(Path,Value,_)|ListOfValues]):-
	set_deep(Path,V,Value),!,
	set_all_atts(V,ListOfValues).

%%% total_offer_cost/3
%%% Computes the total cost of nodes in the Offer as well as the number of nodes.
%%% IS: Ok this could be done recursively, but I am in an all-slutions mood
%%% today.
total_offer_cost(Allocs,Nodes,Cost):-
	findall((N,Cost),
	     (
		   member(dc_in_cluster(_,Cluster,ListOfValues),Allocs),
			 once(member(nodes(N),ListOfValues)),
		   compute_cluster_cost(Cluster,N,Cost)
			 ),
		                   NumCostList),
    findall(X,member((X,_),NumCostList),ListOfNumNodes),
		findall(X,member((_,X),NumCostList),ListOfCosts),
	  sum_list(ListOfNumNodes,Nodes),
		sum_list(ListOfCosts,Cost).

%%% compute_cluster_cost/3
%%% compute_cluster_cost(ClusterName,Vals,Cost)
compute_cluster_cost(Cluster,N,Cost):-
		%once(member(nodes(N),ListOfValues)),
		cost_per_cluster(Cluster,CostPerMachine),
		Cost is N * CostPerMachine,!.
%%% something is missing.
compute_cluster_cost(_,_,0).

update_controller_pop_info(Ans):-
	testbed(TestBed),
	get_deep(['testbed','dc-slice-controller'],TestBed,Controller),
	set_deep(['dc-slice-part','dc-slice-controller'],Ans,Controller),
	get_deep(['testbed','dc-slice-point-of-presence'],TestBed,POP),
	set_deep(['dc-slice-part','dc-slice-point-of-presence'],Ans,POP).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% alloc_vdus(Vdus,AllAllocations)
%%% Creates a list of all possible allocations of vdus to clusters.
alloc_vdus([],[]).
alloc_vdus([VDU|Vdus],[Allocs|AllAllocations]):-
	vdu_in_clusters(VDU,Allocs),
	alloc_vdus(Vdus,AllAllocations).

%%% vdu_in_clusters(VDU,Allocation)
%%% successfuly allocate a VDU to a cluster.
%%% If no cluster accomodates the VDU, VDU fails.
vdu_in_clusters(VDU,Succeeded):-
	findall(Match,(
		testbed_resources(Node),
		match_vdu_to_cluster(VDU,Node,Match)
		),Matches),
	filter_matches(Matches,Succeeded,Failed),
	length(Succeeded,N),
	(N >= 1 -> true ;  (displayFailureReasons(Failed),!,fail) ).


displayFailureReasons([]).
displayFailureReasons([dc_in_cluster(DC,Cluster,Props)|Failed]):-
		write([DC,Cluster]),nl,
		writelist(Props),nl,
		displayFailureReasons(Failed).

%%% testbed_resources(Node)
%%% Reports one single node in the testbed.
testbed_resources(Node):-
	testbed(T),
	member(Node,T.testbed.nodes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% filter_matches/3
%%% filter_matches(AllMatches,Succeeded,Failed)
%%% splits the list of matches to succeessful and failed.
filter_matches([],[],[]).
filter_matches([Match|Matches],[Match|Succeeded],Failed):-
		successful_match(Match),!,
		filter_matches(Matches,Succeeded,Failed).
filter_matches([Match|Matches],Succeeded,[Match|Failed]):-
		filter_matches(Matches,Succeeded,Failed).

%%% successful_match/1
%%% A successful match is one that does not have a failed member.
successful_match(dc_in_cluster(_,_,List)):-
		not(member(failed(_,_,_),List)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Match section
%%% Code that matches request message to provider.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% match_vdu_to_cluster/3
%%% match_vdu_to_cluster(VDU,Cluster,Answer)
%%% Matches based on the list of rules.
match_vdu_to_cluster(VDU,Cluster,dc_in_cluster(VDUName,ClusterName,Updates)):-
	  rules_in(List),
		match_all_rules(List,VDU,Cluster,Updates),
		get_deep(['node_cluster','name'],Cluster,ClusterName),
		get_deep(['dc-vdu',id],VDU,VDUName).

%%% match_all_rules/4
%%% match_all_rules(Rules,VDU,Cluster,Answers)
%%% Matches all rules in the list, against desired VDU values and the values in the
%%% specific Cluster.
%%% Reports all answers for a specific VDU.
/*
The code actually returns all answers even failed ones, so that it can inform
the Broker why the request failed. This can get tricky.
*/

match_all_rules([],_,_,[]).
match_all_rules([R|Rest],VDU,Cluster,Updates):-
		matching_rule(R, DVUField,ClusterField,CompOperator,Carry),
		get_deep(DVUField,VDU,DesiredValue),
    get_deep(ClusterField,Cluster,CValue),
    translate_desired_value(DesiredValue,CompOperator,Operator,VDUValue),
		Call =.. [Operator,CValue,VDUValue],
		Call,
		!,
    match_all_rules(Rest,VDU,Cluster,RestUpdates),
		carry_out(Carry,succeeded(DVUField,CValue,desired(VDUValue)),RestUpdates,Updates).

match_all_rules([R|Rest],VDU,Cluster,Updates):-
		matching_rule(R, DVUField,ClusterField,_CompOperator,_Carry),
		get_deep(DVUField,VDU,DesiredValue),
    get_deep(ClusterField,Cluster,CValue),
    !,
    match_all_rules(Rest,VDU,Cluster,RestUpdates),
		Updates = [failed(DVUField,CValue,desired(DesiredValue))|RestUpdates].


%%% carry_out/4
%%% carry_out(HandlerToken,UpdateValue,RestUpdates,NewListofUpdates)
%%% Depending on the handlerToken processes the value.
%%% Should we keep the value?
carry_out(yes,UP,RestUpdates,[UP|RestUpdates]):-!.
carry_out(no,_,RestUpdates,RestUpdates).
carry_out(nodes,succeeded(_,_,desired(Nodes)),RestUpdates,[nodes(Nodes)|RestUpdates]).

%%% rules_in/1
%%% rules_in(List)
%%% Mactches List with a list of ids of all rules.
rules_in(List):-
		findall(L,matching_rule(L,_,_,_,_),List).

%%% matching_rule/5
%%% matching_rule(ID,RequestPath,ProviderPath,TestConditionOperator,HandlerToken)
%%% ID : Rule ID (can be any atomic)
%%% RequestPath: YAML/JSON path of value in request files
%%% ProviderPath: YAML/JSON path of value in provider files,
%%% TestConditionOperator: Operator under which testing should be done
%%% HandlerToken: How to handle the answer (optimization)
%%% In the case that nodes is defined it reports the term that will store
%%% the necessary value of the resource required.
matching_rule(1,
   ['dc-vdu','epa-attributes','host-epa','memory-mb'],
   ['node_cluster','memory_mb'],
		>=, yes).
matching_rule(2,
   ['dc-vdu','epa-attributes','host-epa','cpu-number'],
   ['node_cluster','cpu_number'],
		>=,yes).
matching_rule(3,
   ['dc-vdu','epa-attributes','host-epa','storage-gb'],
   ['node_cluster','min_storage_gb'],
		>=,yes).
matching_rule(4,
   ['dc-vdu','host-count-in-dc'],
   ['node_cluster','available_nodes'],
		>=,nodes).

% matching_rule(5,
%    ['dc-vdu','epa-attributes','host-epa','host-count'],
%    ['node_cluster','available_nodes'],
% 		_,nodes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Translate section.
%%% Translates verbally described values to  values that can be matched.
%%%%%%%%%%%%%%%%%%%%%%%

%%% translate_desired_value/4
%%% translate_desired_value(DesiredValue,Operator,PrologOperator,ValuetoCompare)
%%% Translates the desidred value that can be verbally described along with
%%% the proposed operator, to the Prolog operator and the Value the latter
%%% should be checked against.
%%% If the value is numeric, then the operator defined in the corresponding
%%% rule should be used.
translate_desired_value(DesiredValue,CompOp,CompOp,DesiredValue):-
		value_type(DesiredValue,numeric),!.

translate_desired_value(DesiredValue,CompOp,CompOp,DesiredValue):-
		value_type(DesiredValue,symbolic),!.

%%% This is for the verbal constraints that appear in dictionaries.
translate_desired_value(DesiredValue,CompOp,Op,Num):-
		is_dict(DesiredValue),!,
		get_dict(Key,DesiredValue,Num),
		value_type(Num,Type),
		verbal_cons(Key,Type,CompOp,Op),
		!.


%%% verbal_cons/4
%%% Handling verbal constraints, by translating them to
%%% the corresponding operators.
%%% Language
verbal_cons(greater_or_equal,numeric,_,>=).
verbal_cons(lower_than_equal,numeric,_,=<).
verbal_cons(greater_than,numeric,_,>).
verbal_cons(lower_than,numeric,_,<).
verbal_cons(equal,numeric,OP,OP).

verbal_cons(in_range,list,_,range_constraint).
verbal_cons(equal,symbolic,_,=).

%%% Determine the Type of each value.
%%% value_type/2
%%% what is the type of the value (numeric, symbolic)
%%% needed to determine the operator.
value_type(Value,numeric):-number(Value),!.
value_type(Value,symbolic):-atom(Value),!.
value_type(Value,list):-is_list(Value),!.

%%% Predicates definingOperators for checking Values.
%%% range_constraint/2
range_constraint(Value,[Min,Max]):-
		number(Value),!,
		Value >= Min,
		Value =< Max.

range_constraint(Value,List):-
		member(Value,List).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Utilities on Prolog Dictionaries
%%% get_deep
%%% get_deep/3
get_deep([Key],Dict,Value):-
		get_dict(Key,Dict,Value).

get_deep([Key|Rest],Dict,Value):-
		get_dict(Key,Dict,NewDict),
		get_deep(Rest,NewDict,Value).

%%% ONLY WORKS FOR EXISTING KEYS
set_deep([Key],Dict,Value):-
		b_set_dict(Key,Dict,Value).

set_deep([Key|Rest],Dict,Value):-
		get_dict(Key,Dict,NewDict),
		set_deep(Rest,NewDict,Value).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% demo_print_functions
demo_print_allocs(All_Allocations):-
	  nl,write('ALL Potential Allocations'),nl,
		member(One,All_Allocations),
		findall((VDU,CLUSTER),member(dc_in_cluster(VDU,CLUSTER,_),One),PairsList),
		write('*** (VDU, Cluster) ***'),nl,
		writelist(PairsList),fail.

demo_print_allocs(_):-
		write('End Potential Allocs'),nl.


%%% writelist/1
writelist([]).
writelist([X|R]):-
		write(X),nl,
		writelist(R).
