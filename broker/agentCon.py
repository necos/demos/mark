# Agent Communication
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Sarantis Kalafatidis, Polyxronis Valsamas, Ilias Sakellariou, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import pika
import time
# Parameter to set the waiting time for a batch of requests sent.
WAIT = 1

class Start_agent_consuming(object):
    def __init__(self,p_hostIP,p_creds):
        self.hostIP = p_hostIP
        self.creds = p_creds
        #self.hostIP='127.0.0.1'
        #self.creds = pika.PlainCredentials('dev','code#rab#2019')
        #-- Basic consume (waiting for responce)
        self.connection_agent = pika.BlockingConnection(
                pika.ConnectionParameters(host=self.hostIP, credentials = self.creds))
        self.channel_agent = self.connection_agent.channel()
        result = self.channel_agent.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue
        self.channel_agent.exchange_declare(exchange='topic_logs', exchange_type='topic')
        #----Create and declare a unic queue for the broker---
        #result = channel_agent.queue_declare('', exclusive=True)
        #self.cor_id=corr_id
        self.response = [] # None
        self.channel_agent.basic_consume(queue=self.callback_queue,
                on_message_callback=self.on_response)
        #while self.response is None:
        #channel_agent.connection.process_data_events(time_limit=5) # 5 second


    #--What to do when i receive a message from agents
    def on_response(self,ch,method,props,body):
        #if self.cor_id == props.correlation_id:
        # IS: The message is byte stream apparently.
        mess = body.decode()
        if mess != '"None"':
            #print('Got a response.')
            self.response.append(body.decode())

    #-----Request to agents------
    def call(self,routing_key,message):
        self.response = [] # None
        self.channel_agent.basic_publish(
            exchange='topic_logs',
            routing_key= routing_key,
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                #correlation_id=corr_id,
            ),
            body=message)

        t_end = time.monotonic() +  WAIT
        #while self.response is None:
        # Collects messages for WAIT seconds
        # could check whether the message is empty as well.
        while time.monotonic() < t_end:
            # Chnaged timeout to 1 sec (faster)
            self.channel_agent.connection.process_data_events(time_limit=1)
        return self.response
