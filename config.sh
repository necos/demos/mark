#!/bin/bash

TARGET="agent broker builder wanagent"
echo "Copying rabbitmq_config.json file to targets"
for i in $TARGET; do
   cp rabbitmq_config.json ./$i/rabbitmq_config.json
done 
