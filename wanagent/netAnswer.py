# File that contains the code for answering Broker Requests.

# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Ilias Sakellariou, Sarantis Kalafatidis, Polyxronis Valsamas, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import copy
from functools import reduce
from marketplace_utilities import *

# Main function for answering net reuests
# message is an anget dictionary that has the request
def net_agent_answer(net_info,message):
    link_info = message["net-slice-part"]["link-ends"]
    ip1 = get_addess_part(link_info,"link-end1-ip")
    ip2 = get_addess_part(link_info,"link-end2-ip")
    links = net_info["wan"]["links"]

    # Stupid
    for link in links:
        if link["link"]["ip1"] == ip1 and link["link"]["ip2"] == ip2:
            break
    # Load the resource file from the json descriptions
    out_message = copy.deepcopy(message)
    out_message["net-slice-part"]["wan-slice-controller"] = net_info["wan"]["wan-slice-controller"]
    out_message["net-slice-part"]["cost"] = link["link"]["cost"]
    print(link)
    #print(json.dumps(message))
    print('------------')
    #print(json.dumps(out_message))

    #print(out_message["net-slice-part"]["cost"])
    return out_message


def get_addess_part(link_info,which):
    return '.'.join([link_info[which].split(".")[0],"0","0","0"])
