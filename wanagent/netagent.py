# Implementation of the Wan Agent
# Developed under the Necos Project (http://www.h2020-necos.eu/)
# Authors: Sarantis Kalafatidis, Polyxronis Valsamas, Ilias Sakellariou, Lefteris Mamatas
# University of Macedonia, Thessaloniki Greece.

import sys
import os
import signal
import json
import subprocess
import pika


#from pyPrologConnection import *
from marketplace_utilities import *
from netAnswer import *
###################################################
# signal section
def terminateAgent():
    sys.exit(1)

signal.signal(signal.SIGHUP, terminateAgent)
signal.signal(signal.SIGTERM, terminateAgent)
###################################################



RMQ_Conf = load_rabbit_config()
hostIP=RMQ_Conf['rabbitmq_ip'] #'127.0.0.1'
creds = pika.PlainCredentials(RMQ_Conf['rabbitmq_user'],RMQ_Conf['rabbitmq_pass'])

#----connecting with RabbitMQ server
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=hostIP, credentials = creds)
    )
channel= connection.channel()

#------Set exchange type------
channel.exchange_declare(exchange='topic_logs', exchange_type='topic')

#----Create and declare a unic queue for each client---
result = channel.queue_declare('', exclusive=True)
queue_name = result.method.queue

#----Create binding (The relationship between exchange and a queue)
binding_keys = sys.argv[2:]
print('Loading Resource File:')
print(sys.argv[1])
net_info = loadFile(sys.argv[1])
# ADD Loading here

if not binding_keys:
    sys.stderr.write("Usage: %s [binding_key]...\n" % sys.argv[0])
    sys.exit(1)

for binding_key in binding_keys:
    channel.queue_bind(
        exchange='topic_logs', queue=queue_name, routing_key=binding_key)
    print(binding_key)

print(' [*] Waiting messages from broker. To exit press CTRL+C')

#---Callback method (what to do when i take a message)---
def callback(ch, method, props, body):
    #print(body)
    #print(" [x] %r:%r" % (method.routing_key, body))
    #pr  = start_prolog('./sliceagent.pl')
    #load_testbed_info(sys.argv[1]) #'./singleTestBed.py')
    # Aparently the messages are received in b'"form"
    message = json.loads(body.decode())
    #print(json.dumps(message["net-slice-part"]["link-ends"]))
    #print(json.dumps(message))
    #final_message_dict = agentAnswer(body.decode())
    final_message = json.dumps(net_agent_answer(net_info,message))
    print("----------------")
    #print(final_message)

    ch.basic_publish(exchange='',
                         routing_key=props.reply_to,
                         properties=pika.BasicProperties(correlation_id = \
                                                             props.correlation_id),
                         body=final_message)


#------Basic consume (waiting for responce)-----
channel.basic_consume(
    queue=queue_name, on_message_callback=callback, auto_ack=True)

channel.start_consuming()
#channel.basic_qos(prefetch_count=1)
