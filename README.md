# MARK (Resource Discovery via the Marketplace)
This file provides guidance to install and run the MARK demo.

Copyright 2019 NECOS Consortium
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file and software referenced except in compliance with the License.
You may obtain a copy of the License at (http://www.apache.org/licenses/LICENSE-2.0).

A video of the Single Host Demo execution can be found [here](https://youtu.be/T7yX_-d1_F0). 

A video of a demonstration invovling FED4FIRE experimentation testbeds as described in D6.2 can be found [here](https://www.youtube.com/watch?v=ykzrZu8Gstk).


# Introduction 
The main objective of the marketplace demo (MARK) is to demonstrate that the marketplace concept introduced in NECOS as a dynamic resource discovery mechanism over multiple geographically distributed resource providers. The Slice Builder forwards a Slice request (PDT message) to the Slice Broker, that in turn decomposes the latter and addresses slice part queries to the agents. Agents answer back with cost annotated availability and their answers are aggregated by the broker and send as a single SRA message back to the Builder. The current implementation contains a partial version of the Builder that extracts statistical data (e.g. number of alternative offers, etc) of the SRA message. Details can be found in [NECOS](http://www.h2020-necos.eu/) Project [deliverables](http://www.h2020-necos.eu/documents/deliverables/), and in particular the structure of the PDT and SRA messages, the slice representation as well as a more detailed description of the message exchange in the marketplace can be found in deliverable D4.2, the mechanisms used to compute agent answers to requests in deliverable D5.2 and a description of a demo involving distributed FED4FIRE open experimentation testbeds in deliverable D6.2.


## Functional Components
The components of the NECOS regarding the Marketplace are the following:

- **Slice Builder**: This component is an implementation of the following functionalities of the Necos Slice Builder: initiation of the resource discovery process (i.e. it requests resources for slice parts), collection of results (i.e. the SRA Message) and computation using a brute force mechanism of the final slice selection, i.e. it computes the minimum cost slice. 
- **Slice Broker**: Implementation of the Slice Broker, the component responsible to decompose the PDT message received by the Slice Builder to slice part queries and address and collect agent answers to each such query. Thus, the Slice Broker supports a search mechanism for requesting resources from infrastructure providers that includes the formulation of requests to potential network resource providers based on DC resource offers. The Broker finally compiles all DC/WAN agent answers to an SRA message addressed to the builder, the latter listing alternative resources for each slice part. It should be noted that the Slice agent registration is handled by the RabbitMQ server. 
- **DC-Slice Agent**: DC Provider agent that matches resource requests to available resources and returns a cost annotated answer to the Broker. In the case of FED4Fire DC Agents, the component checks the current availability of resources in the testbed (that is considered a provider). It should be noted that the current implementation of the agent, reports back the minimum cost infrastructure offer for a requested slice part.  
- **WAN-Slice Agent**: WAN Provider agent, replying to connectivity requests.


## Structure of the Repository
- `/agent`: Implementation of the DC Slice Agent. The directory also contains the runservers.sh bash script to start a number of agents on different resource description files.
- `/agent/providers`: Directory containing “semi-artificial” resource files for DC providers. 
- `/agent/f4f_testbeds`: Directory containing resources files regarding Fed4Fire testbeds (snapshots),
- `/agent/testbedExtractor`: Directory containing code to contact APIs of Fed4Fire testbeds, in order to obtain an up-to-date view of resource availability. 
- `/wanagent`: Implementation of the WAN Slice Agent. The directory also contains the runwanservers.sh bash script to start a number of agents on different resource description files.
- `/broker`: Implementation of the Slice Broker. To run: python3 broker.py.
- `/builder`: Implementation of the Slice Builder. To run: python3 builder.py.
- `/builder/requests`: Example slice requests. 

`rabbitmq_config.json`: file containing the credentials of the rabbitMQ messaging platform, i.e. the Host IP of the RabbitMQ server, and a valid username and password. 

## How to install MARK
In order to run the MARK demo in a Linux machine it is necessary to: 
- Clone the repository on your machine (more detailed instructions are provided in the GitLab page). You can clone the repository in any directory on your machine: 
`git clone https://gitlab.com/necos/demos/mark.git`

- Have an active installation of RabbitMQ messaging platform (not necessarily on the same host), along with the credentials to access it;
- Have Python3 installed (preferably version 3.6 and above) along with the following packages: sys, os, signal, json, subprocess, pika , pyswip, functools, copy, threading; and
- Have SWI-Prolog (https://www.swi-prolog.org/) installed. This is required ONLY for DC agents, i.e. if you place components on different hosts, then SWI-Prolog needs to be installed only on those that host DC agents. 

The only configuration necessary regards the credentials of the RabbitMQ installation. Edit the rabbitmq_config.json, setting the credentials with your host IP, username and password, as shown below:

`{ "rabbitmq_ip":"<The IP of the host where RabbitMQ runs>",
  "rabbitmq_user":"<RabbitMQ user name>",
"rabbitmq_pass":"<RabbitMQ password"
}`
    
This file must be copied to all the directories containing components of the marketplace, i.e. the `/agent`, `/wanagent`, `/broker` and `/builder` directories. To facilitate the process, the `config.sh` bash script is provided (simply update  the above info and run `config.sh`).

## Running MARK
- **Step 1**: Ensure that your RabbitMQ messaging server is running and that your credentials are correct.

- **Step 2**: Start DC Slice agents. In order to start the slice agents, go to the /agent directory and use the bash script runservers.sh that will start as many providers as the number of resource files in the directory /agents/providers. Alternatively, you can start a single provider by issuing the command: `python3 agent.py <resource file.json>`

- **Step 3**: Start WAN Slice agents. Go to the /wanagent directory and run the ./runwanservers.sh bash shell script. Alternatively you can start each agent by `python3 netagent.py "<WAN resource Provider File.json>" "wan"`

- **Step 4**: Start the Slice Broker. Goto the ./broker directory and issue the command `python3 broker.py`
  
*NOTE: Steps 2-4 can be done in any order.*

- **Step 5**: Start the Slice Builder. Starting the Slice Builder takes a Slice Request Description file (PDT Message) as an argument, implying that this is the request to be answered by the marketplace. As soon as the builder is executed this request is sent to the broker. Upon the reception of the alternative slice parts, the builder searches (brute force) for the lowest cost slice and reports on various statistics regarding the request. For instance, an example output of the builder is the following: 

`DC Slice Parts, 3 ,Net Slice Parts, 2 ,Services, 6 ,Wall Time Elapsed, 17.534036331999232 ,Rec-dc-slice-parts, 15 ,Rec-net-slice-parts, 42 ,Number of Alternative Slices, 360`

`Final Selection: `

`('dc-slice-part1', 'dcProv1')`

`('dc-slice-part2', 'dcProv19')`

`('dc-slice-part3', 'dcProv13')`

`('dc-slice-part1-to-dc-slice-part2', 'netProv1')`

`('dc-slice-part1-to-dc-slice-part3', 'netProv1')`

`cost:  7.582294808515527`

reporting on alternatives of a slice containing 3 DC and 2 WAN (Net) slice parts. The total time it took the marketplace to compute the answer is reported on Wall Time Elapsed, and the number of alternatives slice parts is indicated in the Rec-* fields, along with the number of alternative slices that can be built. “Final Selection” is the lower cost slice that the builder has selected. 


## Running MARK on multiple hosts

MARK is a lightweight multi-agent system, where the described components communicate over TCP/IP. This approach (and obviously the current implementation) allows for running the marketplace components in a varying number of hosts. In the simplest case, the marketplace can be run in a single machine that will host all the components (i.e. please see video referenced in the introductory section). Alternatively, each directory (i.e. component) listed above can be copied to a different machine, thus having 1 host for the builder component, 1 host for the broker component and N hosts for the agents, where N can be any number (> 3, assuming 2 DC and 1 WAN agent connecting them). For instance, one can have 20 DC/WAN-agents, 1 Broker and 1 Builder yielding a total of 22 machines in an experiment. To run distributedly no changes are required to the code.

## References

If you use the code for your own experimentation please reference one of the following papers: 
- P. D. Maciel Jr., F. L. Verdi, P. Valsamas, I. Sakellariou, L. Mamatas, S. Petridou, P. Papadimitriou, D. Moura, A. I. Swapna, B. Pinheiro, S. Clayman, *“A Marketplace-based Approach to Cloud Network Slice Composition Across Multiple Domains”* in Proceedings of the 2nd Workshop on Advances in Slicing for Softwarized Infrastructures (S4SI), co-hosted at the 5th IEEE NetSoft, 28 June 2019, Paris

- P. Valsamas, P. Papadimitriou, I. Sakellariou, S. Petridou, L. Mamatas, S. Clayman, F. Tusa, A. Galis, *“Multi-PoP Network Slice Deployment: A Feasibility Study”* in IEEE CloudNet 2019, Nov. 2019, Coimbra, Portugal

### Software Statistics
The following information concerns statistics regarding the code in the MARK demo:

- the **Agent** and the **Wan Agent** are in total 1456 lines of code,
- the **Builder** is 249 lines of code,
- the **Broker** is 200 lines of code 

for  total of 1905 lines of code.


