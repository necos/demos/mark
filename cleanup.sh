#!/bin/bash

TARGET="agent broker builder wanagent"
echo "Cleaning Up Python files"
for i in $TARGET; do
   cd $i
   py3clean . 
   cd .. 
done 
